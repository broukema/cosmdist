/* 
   cosmdist - standard FLRW cosmological distance functions

   Copyright (C) 2004 Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

*/

/*  
    get comoving distance  as a function 
    of redshift, or inversely,  or get cosmological time or inversely 
*/

#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

#ifndef EXIT_FAILURE
# define EXIT_FAILURE 1
#endif

#include "cosmdist.h"

/* 
   cosm_initialise_pointers will be set to  COSM_INITIALISE_POINTERS 
   in order to show that pointers have been initialised.
*/
#define COSM_INITIALISE_POINTERS 123454321  /* arbitrary unusual integer */
static int  cosm_initialise_pointers;


struct cosm_param_chain_element *p_chain ;  /*  local to this file */

/* the following must be static since they must be remembered between
   successive calls to cosmdist functions */
static struct cosm_param_chain_element *p_first = 
  NULL; /* first pointer to chain */
static struct cosm_param_chain_element *p_last ; /* last pointer in chain */

/* 
   (1) Find out if a set of local cosmological parameters has already 
   been called at least once - i.e. find out if the integrations and
   spline parameters have already been calculated.

   (2) If not, call cosm_init  in order to initialise these integrations.
   
   (3) In either case, modify *p_chain (pointed to by  **p_chain) 
   so that the function knows which spline information to use 
   to evaluate the required value.
*/
   


/* 
   Find out if the set of input local cosmological parameters has 
   already been used to initialise the time and distance integrals.
   
   If yes, then the pointer of type  cosm_param_chain_element is returned
   and the return value is 0.
   
   If no, then the return value is 1. No action to create a new
   element is taken in this function.
*/

int cosm_get_params_index( struct cosm_struct_local_params p_input,
			   struct cosm_param_chain_element ** p_p_chain) {

  struct cosm_param_chain_element *p;
  int found = 0; 
  int iparams = 0;   
  int want_debug = (0 || COSM_WANT_DEBUG);  /* local */
  
  /* 
     p_first needs to be saved between successive calls of functions in 
     this file: cosm_get_params_index, cosm_check_initialisation
     and cosm_get_comov_dist, cosm_get_*_["dist"||"time"] 
  */

  p = p_first; /* set pointer to first element in chain */

  if(want_debug)
    {
      printf("cosm_get_params_index: p_chain = %p  p_first = %p\n",
	     *p_p_chain,p_first);
    };
  

  /* There is no need to test all parameters in the set; if one
     parameter fails to be close enough, then the others in that 
     chain element can be ignored: found should remain as 0, i.e. false. 
  */

  while (p != NULL && !found && iparams < MAX_COSM_PARAMS) 
    {
      if(want_debug)
	{
	  printf("cosm_get_params_index: p= %p  iparams = %d\n",
		 p,iparams);
	  printf("p->next = %p\n",p->next);
	  printf("(p->params).omm = %.2f  p_input.omm = %.2f \n",
		 (p->params).omm, p_input.omm);
	};
      if ( fabs( ((p->params).H_0) - (p_input.H_0) ) 
	   < COSM_SAME_TOLERANCE ){
	if ( fabs( ((p->params).omm) - (p_input.omm) ) 
	     < COSM_SAME_TOLERANCE ){
	  if ( fabs( ((p->params).omlam) - (p_input.omlam) ) 
	       < COSM_SAME_TOLERANCE ){
	    if ( fabs( ((p->params).omrad) - (p_input.omrad) ) 
	       < COSM_SAME_TOLERANCE ){
	      if ( fabs( ((p->params).w_0) - (p_input.w_0) ) 
		   < COSM_SAME_TOLERANCE ){
		found= 1;
		/*	      p_chain= p; */
		*p_p_chain = p;  /* p_p_chain points to p_chain */
	      };
	    };
	  };
	};
      };

      p = p->next;  /* go to next element in chain */
      iparams++;
    };  /*   while (p != NULL && !found && iparams < MAX_COSM_PARAMS)   */

  if(want_debug)
    {
      printf("found = %d  *p_p_chain = %p\n", found,*p_p_chain);
    };


  /*  p_found=*p_p_chain; */

  if (!found) 
    {
      return 1;
    }
  else
    { 
      return 0;
    };
  
}




int cosm_check_initialisation( 
			      struct cosm_struct_local_params 
			      p_input,  /* INPUT */
			      struct cosm_param_chain_element 
			      ** p_p_chain /* OUTPUT */
			      )
{
  int i_get_params;    
  int i_cosm_init;   /* error flag on cosm_init( ... ) */

  int  want_debug = (0|| COSM_WANT_DEBUG);  /* local to this routine */
  
  /* double aa = 0.25, dd ; */ /* local scale factor and distance */

  p_chain= NULL;

  if(want_debug){
    printf("\n");
    printf("Will call cosm_get_params_index.\n");
      };

  if(cosm_initialise_pointers != COSM_INITIALISE_POINTERS)
    {
      cosm_initialise_pointers = COSM_INITIALISE_POINTERS;
      p_first = NULL;
      p_last = NULL;
      if(want_debug){
	printf("- pointers initialised.\n");
      };
    };


  if(want_debug){
    printf("next line.\n");
  };

  if(p_first == NULL)  /* if the chain has not yet been started */
    {
      p_chain= malloc( SIZE_OF_COSM_PARAM_CHAIN_ELEMENT +
			SIZE_OF_COSM_STRUCT_LOCAL_PARAMS );
      if(NULL==p_chain)perror("cosmdist::get_dist ERROR: cannot allocate memory during initialisation.\n");

      /* start chain */
      p_chain->next= NULL;
      p_first= p_chain;
      p_last= p_chain;

      (p_chain->params).H_0= p_input.H_0;
      (p_chain->params).omm= p_input.omm;
      (p_chain->params).omlam= p_input.omlam;
      (p_chain->params).omrad= p_input.omrad;
      (p_chain->params).w_0= p_input.w_0;

      if(want_debug)
	{
	  printf("p_first was found to be NULL\n");
	  printf(" (p_chain->params).H_0= p_input.H_0 are %.2f  %.2f \n",
		 (p_chain->params).H_0, p_input.H_0);
	  printf("The address of the cosm parameter set seems to be ");
	  printf("p_chain = %p \n",  p_chain); 
	};

      i_cosm_init = cosm_dist_init( p_chain );

      if(want_debug)
	{
	  printf("cosm_get_dist: after cosm_dist_init initialisation\n");
	  
	  /*
	    dd= 
	    gsl_spline_eval( p_chain->spline_object_comov, 
	    aa, p_chain->spline_accel_comov );

	    printf("distance to a = %.4f is  %.2f  mpc \n", aa, dd);
	  */
	};

    }
  else       /* if the chain *has* already been started */
    {
      i_get_params= cosm_get_params_index( p_input, 
				&p_chain );

      if(want_debug)
	{
	  printf("return value of cosm_get_params_index is %d\n", 
		 i_get_params);
	  printf("the address of the cosm parameter set seems to be ");
	  printf("p_chain = %p \n",  p_chain);
	};

      if(p_chain != NULL)
	{
	  if(want_debug)
	    {
	      printf("Found cosm parameters: \n");
	      printf("H_0 = %.2f  omm = %.2f  omlam = %.2f  omrad = %.2f  w_0 = %.4f \n",
		     (p_chain->params).H_0, 
		     (p_chain->params).omm, 
		     (p_chain->params).omlam, 
		     (p_chain->params).omrad, 
		     (p_chain->params).w_0);
	    };
	};
      
      if(i_get_params)  /* if no old entry found */
	{
	  p_chain= malloc( SIZE_OF_COSM_PARAM_CHAIN_ELEMENT  
			    + SIZE_OF_COSM_STRUCT_LOCAL_PARAMS );
      if(NULL==p_chain)perror("cosmdist::get_dist ERROR: cannot allocate memory for new metric parameter set.\n");

	  /* add to chain */
	  p_chain->next= NULL;

	  p_last->next= p_chain;
	  p_last= p_chain;

	  (p_chain->params).H_0= p_input.H_0;
	  (p_chain->params).omm= p_input.omm;
	  (p_chain->params).omlam= p_input.omlam;
	  (p_chain->params).omrad= p_input.omrad;
	  (p_chain->params).w_0= p_input.w_0;

	  /* for the moment, nothing for splines */	

	  if(want_debug)
	    {
	      printf("no old entry found\n");
	      printf("(p_chain->params).H_0 = %.2f \n",(p_chain->params).H_0);
	      printf("The address of the this cosm parameter set is %p \n", 
		     p_chain);
	      printf("Some of its values include  next= %p ",(*p_chain).next );
	      printf("H_0= %.2f  w_0 = %.4f \n", 
		     (*p_chain).params.H_0, (*p_chain).params.w_0 
		     );
	    };

	  i_cosm_init = cosm_dist_init( p_chain );   

	}
      else
	{
	  if(want_debug)
	    {
	      printf("an old entry was found\n");
	      printf("(p_chain->params).H_0 = %.2f \n",(p_chain->params).H_0);
	      printf("The address of the this cosm parameter set is %p \n", 
		     p_chain);
	    };
	};
      
    };  /*   if(p_first == NULL)    else */
  
      

  if(want_debug)
    {
      printf("The value NULL is %p\n",NULL);
      printf("The address of the first cosm parameter set is %p \n", p_first);
    };

  /*  
      Assign p_chain using its own pointer so that the calling routine
      knows where to find the (possibly newly) initialised set of cosm
      parameters and spline values for interpolation of the integral.
  */

  *p_p_chain= p_chain;

  return (0);
}  /*  end of cosm_check_initialisation  */





/* free up allocated memory used by the cosm parameter chains */
int cosm_dist_free(void) 
{
  int i_get_params;
  int  want_debug = (0|| COSM_WANT_DEBUG);  /* local to this routine */
  int n_chain_elements = 0;
  
  /* double aa = 0.25, dd ; */ /* local scale factor and distance */

  p_chain= p_first; /* point to beginning of chain */

  if(COSM_INITIALISE_POINTERS!=cosm_initialise_pointers)
    {
      return 0; /* nothing to be freed */
    }
  else    
    {
      while(NULL!=p_chain && n_chain_elements < MAX_COSM_PARAMS){
	n_chain_elements ++;

	/* store the address of the next element before freeing up 
	   the element of the chain pointed to by p_chain
	 */
	p_first = p_chain->next; 

	/* free up allocated memory: */
	/*  GSL objects pointed to by this element */
	gsl_spline_free(p_chain->spline_object_comov);
	gsl_interp_accel_free(p_chain->spline_accel_comov);
	gsl_spline_free(p_chain->spline_object_comov_inv);
	gsl_interp_accel_free(p_chain->spline_accel_comov_inv);

	gsl_spline_free(p_chain->spline_object_time);
	gsl_interp_accel_free(p_chain->spline_accel_time);
	gsl_spline_free(p_chain->spline_object_time_inv);
	gsl_interp_accel_free(p_chain->spline_accel_time_inv);

	/* free up the element itself */
	free(p_chain);
	p_chain = p_first; /* the chain is now shorter */
      };
    };
  if(want_debug){
    printf("- pointers freed.\n");
  };

  if(NULL!=p_chain || n_chain_elements > MAX_COSM_PARAMS){
    printf("cosm_dist_free WARNING: final chain element free up\n");
    printf("is  p_chain = %p  and n_chain_elements = %d, MAX_COSM_PARAMS = %d\n",
	   p_chain, n_chain_elements, MAX_COSM_PARAMS);
    return 1;
  };

  return 0;
}  /*  end of cosm_dist_free  */



/*
  This function first checks if the input local cosm parameters 
  require an initialisation of the integrals and splines, 
  and then evaluates the function.
*/



double cosm_get_comov_dist( double H_0, double omm, double omlam, 
			    double omrad, double w_0, 
			    double redshift
			    )
{
  /* put local cosm parameters into a structure */
  struct cosm_struct_local_params  p_input;

  /*  struct cosm_param_chain_element *p_chain ; */
  int want_debug = (0 || COSM_WANT_DEBUG) ;     /* local */
  int i_cosm_check_initialisation; 
  double  zz, dd;   
  double  aa; /* scale factor input */


  if(want_debug)
    {
      printf("cosm_get_comov_dist: first line\n");
    };


  /* statement section */
  p_input.H_0= H_0;
  p_input.omm= omm;
  p_input.omlam= omlam;
  p_input.omrad= omrad;
  p_input.w_0= w_0;


  if(redshift < -COSM_TOLERANCE)
    {
      errno= EDOM;  /* negative redshifts are in the future */
      zz= 0.0;
    }
  else
    {
      zz= redshift;
    };

  aa= 1.0/(1.0+ zz);

  i_cosm_check_initialisation = 
    cosm_check_initialisation( p_input, 
			       &p_chain
			       );
  if(want_debug)
    {
      printf("Based on the p_chain = %p  address we have:\n",p_chain);
      printf("(p_chain->params).H_0 = %.2f \n",(p_chain->params).H_0);
    };

   /* evaluate spline using the  cosm_param_chain_element spline 
      stuff pointed to by   p_chain  */

  dd= gsl_spline_eval( p_chain->spline_object_comov, 
		       aa, p_chain->spline_accel_comov );

  return dd; 

   /*
     if(want_debug)
     {
       printf("distance to a = %.4f is  %.2f  mpc \n", aa, dd);
     };
   */
   
}


double cosm_get_comov_dist_inv( double H_0, double omm, 
				double omlam, 
				double omrad, 
				double w_0, 
				double distance
				)
{
  /* put local cosm parameters into a structure */
  struct cosm_struct_local_params  p_input;

  /*  struct cosm_param_chain_element *p_chain ; */
  int want_debug = (0 || COSM_WANT_DEBUG) ;     /* local */
  int i_cosm_check_initialisation; 
  double  dd;   
  double  aa,zz; /* scale factor, redshift output */



  /* statement section */
  p_input.H_0= H_0;
  p_input.omm= omm;
  p_input.omlam= omlam;
  p_input.omrad= omrad;
  p_input.w_0= w_0;


  if(distance < -COSM_TOLERANCE)
    {
      errno= EDOM;  /* negative redshifts are in the future */
      dd= 0.0;
    }
  else 
    {
      dd= distance;
    };

  i_cosm_check_initialisation = 
    cosm_check_initialisation( p_input, 
			       &p_chain
			       );


  if(distance > (p_chain->params).horizon)
    {
      errno = EDOM;  /* warning that distance is too big */
      dd= (1-COSM_TOLERANCE) * (p_chain->params).horizon;
      if(want_debug)
	{
	  printf("get_dist: errno = %d  dd=%.2f \n",errno,dd);
	};
    };


  if(want_debug)
    {
      printf("Based on the p_chain = %p  address we have:\n",p_chain);
      printf("(p_chain->params).H_0 = %.2f \n",(p_chain->params).H_0);
    };

   /* evaluate spline using the  cosm_param_chain_element spline 
      stuff pointed to by   p_chain  */

  aa= gsl_spline_eval( p_chain->spline_object_comov_inv, 
		       dd, p_chain->spline_accel_comov_inv );

  if(aa < COSM_TOLERANCE)
    {
      zz = 1.0/COSM_TOLERANCE;
    }
  else
    {
      zz= 1.0/aa - 1.0;
    };

  return zz; 
   
}


double cosm_get_comov_time( double H_0, double omm, 
			    double omlam, 
			    double omrad, 
			    double w_0, 
			    double redshift
			    )
{
  /* put local cosm parameters into a structure */
  struct cosm_struct_local_params  p_input;

  /*  struct cosm_param_chain_element *p_chain ; */
  int want_debug = COSM_WANT_DEBUG ;     /* local */
  int i_cosm_check_initialisation; 
  double  zz, tt;   
  double  aa; /* scale factor input */


  /* statement section */
  p_input.H_0= H_0;
  p_input.omm= omm;
  p_input.omlam= omlam;
  p_input.omrad= omrad;
  p_input.w_0= w_0;


  if(redshift < -COSM_TOLERANCE)
    {
      errno= EDOM;  /* negative redshifts are in the future */
      zz= 0.0;
    }
  else
    {
      zz= redshift;
    };

  aa= 1.0/(1.0+ zz);

  i_cosm_check_initialisation = 
    cosm_check_initialisation( p_input, 
			       &p_chain
			       );
  if(want_debug)
    {
      printf("Based on the p_chain = %p  address we have:\n",p_chain);
      printf("(p_chain->params).H_0 = %.2f \n",(p_chain->params).H_0);
    };

   /* evaluate spline using the  cosm_param_chain_element spline 
      stuff pointed to by   p_chain  */

  tt= gsl_spline_eval( p_chain->spline_object_time, 
		       aa, p_chain->spline_accel_time );

  return tt; 

   /*
     if(want_debug)
     {
       printf("time to a = %.4f is  %.2f  mpc \n", aa, tt);
     };
   */
   
}


double cosm_get_comov_time_inv( double H_0, double omm, 
				double omlam, 
				double omrad, 
				double w_0, 
				double time
				)
{
  /* put local cosm parameters into a structure */
  struct cosm_struct_local_params  p_input;

  /*  struct cosm_param_chain_element *p_chain ; */
  int want_debug = COSM_WANT_DEBUG ;     /* local */
  int i_cosm_check_initialisation; 
  double  tt;   
  double  aa,zz; /* scale factor, redshift output */


  /* statement section */
  p_input.H_0= H_0;
  p_input.omm= omm;
  p_input.omlam= omlam;
  p_input.omrad= omrad;
  p_input.w_0= w_0;


  if(time < -COSM_TOLERANCE)
    {
      errno = EDOM;  /* negative redshifts are in the future */
      tt= 0.0;
    }
  else 
    {
      tt= time;
    };


  i_cosm_check_initialisation = 
    cosm_check_initialisation( p_input, 
			       &p_chain
			       );

  if(time > (p_chain->params).t_0)
    {
      errno= EDOM;  /* negative redshifts are in the future */
      tt= (1-COSM_TOLERANCE) * (p_chain->params).t_0;
    };


  if(want_debug)
    {
      printf("Based on the p_chain = %p  address we have:\n",p_chain);
      printf("(p_chain->params).H_0 = %.2f \n",(p_chain->params).H_0);
    };

   /* evaluate spline using the  cosm_param_chain_element spline 
      stuff pointed to by   p_chain  */

  aa= gsl_spline_eval( p_chain->spline_object_time_inv, 
		       tt, p_chain->spline_accel_time_inv );

  if(aa < COSM_TOLERANCE)
    {
      zz = 1.0/COSM_TOLERANCE;
    }
  else
    {
      zz= 1.0/aa - 1.0;
    };

  return zz; 
   
}



/* present curvature radius  R_C_0 i.e. at t_0 only */
double cosm_get_R_C( double H_0, double omm, 
		     double omlam, 
		     double omrad, 
		     double w_0
		     )
{
  /* put local cosm parameters into a structure */
  struct cosm_struct_local_params  p_input;

  /*  struct cosm_param_chain_element *p_chain ; */
  /* int want_debug = COSM_WANT_DEBUG ;  */   /* local */
  int i_cosm_check_initialisation; 

  double R_C;

  /* statement section */
  p_input.H_0= H_0;
  p_input.omm= omm;
  p_input.omlam= omlam;
  p_input.omrad= omrad;
  p_input.w_0= w_0;


  i_cosm_check_initialisation = 
    cosm_check_initialisation( p_input, 
			       &p_chain
			       );
  R_C= (p_chain->params).R_C;

  if( (p_chain->params).k_curv == 0 )
    {
      errno= ERANGE;   /* R_C is undefined ("infinite") in a flat universe */
    };
  
  return R_C; 
   
}


double cosm_get_horizon( double H_0, double omm, 
			 double omlam, 
			 double omrad, 
			 double w_0
			 )
{
  /* put local cosm parameters into a structure */
  struct cosm_struct_local_params  p_input;

  /*  struct cosm_param_chain_element *p_chain ; */
  /* int want_debug = COSM_WANT_DEBUG ; */    /* local */
  int i_cosm_check_initialisation; 

  double horizon;

  /* statement section */
  p_input.H_0= H_0;
  p_input.omm= omm;
  p_input.omlam= omlam;
  p_input.omrad= omrad;
  p_input.w_0= w_0;


  i_cosm_check_initialisation = 
    cosm_check_initialisation( p_input, 
			       &p_chain
			       );
  horizon= (p_chain->params).horizon;
  
  return horizon; 
   
}



/*  fortran wrappers */

double DOFZ(
	    double *H_0, double *omm, 
	    double *omlam, 
	    double *omrad, 
	    double *w_0, 
	    double *redshift 
	    )
{
  double x;

  /*
    printf("DOFZ: first line\n");
    printf("H_0 = %.4f  omm= %.2f ...\n",*H_0,*omm);
  */

  errno=0;
  x=  cosm_get_comov_dist( *H_0, *omm, 
			   *omlam, 
			   *omrad, 
			   *w_0, 
			   *redshift
			   );
  if(errno != 0)
    perror("ERROR during call to dofz");


  /*
    printf("DOFZ: x= %.4f",x);
  */

  return x;
}


double ZOFD(
	    double *H_0, double *omm, 
	    double *omlam, 
	    double *omrad, 
	    double *w_0, 
	    double *distance
	    )
{
  double x;
  
  errno=0;
  x=  cosm_get_comov_dist_inv( *H_0, *omm, 
			       *omlam, 
			       *omrad, 
			       *w_0, 
			       *distance
			       );
  if(errno != 0)
    perror("ERROR during call to zofd");

  return x;
}


double DPMOFZ(
	    double *H_0, double *omm, 
	    double *omlam, 
	    double *omrad, 
	    double *w_0, 
	    double *redshift
	    )
{
  double x;

  errno=0;
  x=  cosm_get_pm_dist( *H_0, *omm, 
			*omlam, 
			*omrad, 
			*w_0, 
			*redshift 
			);
  if(errno != 0)
    perror("ERROR during call to dpmofz");

  return x;
}


double ZOFDPM(
	    double *H_0, double *omm, 
	    double *omlam, 
	    double *omrad, 
	    double *w_0, 
	    double *distance
	    )
{
  double x;

  errno=0;
  x=  cosm_get_pm_dist_inv( *H_0, *omm, 
			    *omlam, 
			    *omrad, 
			    *w_0, 
			    *distance 
			    );
  if(errno != 0)
    perror("ERROR during call to zofdpm");

  return x;
}

double TOFZ(
	    double *H_0, double *omm, 
	    double *omlam, 
	    double *omrad, 
	    double *w_0, 
	    double *redshift
	    )
{
  double x;

  errno=0;
  x=  cosm_get_comov_time( *H_0, *omm, 
			   *omlam, 
			   *omrad, 
			   *w_0, 
			   *redshift 
			   );
  if(errno != 0)
    perror("ERROR during call to tofz");

  return x;
}


double ZOFT(
	    double *H_0, double *omm, 
	    double *omlam, 
	    double *omrad, 
	    double *w_0, 
	    double *distance
	    )
{
  double x;

  errno= 0;
  x=  cosm_get_comov_time_inv( *H_0, *omm, 
			       *omlam, 
			       *omrad, 
			       *w_0, 
			       *distance
			       );
  if(errno != 0)
    perror("ERROR during call to zoft");
  return x;
}




double RCURV(
	    double *H_0, double *omm, 
	    double *omlam, 
	    double *omrad, 
	    double *w_0 
	    )
{
  double x;

  errno= 0;
  x=  cosm_get_R_C( *H_0, *omm, 
		    *omlam, 
		    *omrad, 
		    *w_0 
		    );
  if(errno != 0)
    perror("ERROR during call to rcurv");
  return x;
}


double RHORIZ(
	    double *H_0, double *omm, 
	    double *omlam, 
	    double *omrad, 
	    double *w_0 
	    )
{
  double x;

  errno= 0;
  x=  cosm_get_horizon( *H_0, *omm, 
			*omlam, 
			*omrad, 
			*w_0 
			);
  if(errno != 0)
    perror("ERROR during call to rhoriz");
  return x;
}




