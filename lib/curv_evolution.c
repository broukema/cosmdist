/*
   cosmdist - standard FLRW cosmological distance functions

   Copyright (C) 2004, 2020 Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

*/

#include "cosmdist.h"

int curv_evolution(double H_0, double omm, double omlam,
                   double omrad,  double w_0){

#define MAX_A 200
  double aa[MAX_A];
  double redshift[MAX_A];
  double aa2 = 1e-5;
  double aa0 = 1.1;
  double daa_log;

  double adot[MAX_A];
  double hh[MAX_A];
  double rho_c_norm[MAX_A];
  double omtot[MAX_A];
  double om_kplus[MAX_A]; /* use non-common convention Omtot-1 */
  double om_kminus[MAX_A]; /* common convention 1-Omtot */
  double r_c2[MAX_A];

  double del_aa, addot[MAX_A];


  int i;
  int want_verbose = 0;


  /* put local cosm parameters into a structure */
  struct cosm_struct_local_params  p_input;

  /*  struct cosm_param_chain_element *p_chain ; */ /*  local to this file */

  /*  struct cosm_param_chain_element *p_chain ; */
  /* int want_debug = COSM_WANT_DEBUG ;  */   /* local */
  int i_cosm_check_initialisation;


  /* statement section */
  /*
  p_input.H_0= H_0;
  p_input.omm= omm;
  p_input.omlam= omlam;
  p_input.omrad= omrad;
  p_input.w_0= w_0;

  i_cosm_check_initialisation =
    cosm_check_initialisation( p_input,
                               &p_chain
                               );
  */


  /* equal logarithmic intervals for scale factor aa */
  daa_log = (log10(aa0)-log10(aa2))/(double)MAX_A;
  for(i=0;i<MAX_A;i++){
    aa[i] = exp(log(10.0)*( log10(aa2) + ((double)i+0.5)*daa_log ));
    redshift[i] = 1.0/aa[i] -1.0;
  };


  printf("\n");

  /* evaluate scale factor aa and other derivative parameters */
  for(i=0;i<MAX_A;i++){
    errno=0;
    adot[i]= cosm_da_dt(H_0, omm, omlam,omrad, w_0,
                    aa[i]);
    if(errno != 0)
      perror("ERROR during cosm_da_dt");

    hh[i] = adot[i]/aa[i];
    rho_c_norm[i] = hh[i]*hh[i]/(H_0*H_0);
    omtot[i]= (
               omm/(aa[i]*aa[i]*aa[i])  +
               omrad/(aa[i]*aa[i]*aa[i]*aa[i]) +
               omlam * pow(aa[i], -(3.0+ 3.0* w_0))
               )/rho_c_norm[i];
    om_kplus[i] = omtot[i] - 1.0;
    om_kminus[i] = -om_kplus[i];

    r_c2[i] = COSM_C_ON_H_0_MPC*COSM_C_ON_H_0_MPC
      *H_0*H_0
      /( aa[i]*aa[i] *hh[i]*hh[i] * om_kplus[i] );

    printf("evn: %12.7f %10.2f %11.5g %11.5g %11.3g %11.5f %11.5g %11.5g\n",
           aa[i],redshift[i],
           adot[i],
           hh[i],
           rho_c_norm[i],
           omtot[i], om_kplus[i],
           r_c2[i]);

    if(want_verbose){
      printf("adot is %.5g /Gyr \n", adot[i]);
      /* H = adot/a */
      printf("adot/a is %.5f /Gyr \n", hh[i]);
      /* rho_c/rho_c_0 = H^2 */
      printf("rho_c/rho_c_0 is %.5g  \n", rho_c_norm[i]);
    };
  };

};
