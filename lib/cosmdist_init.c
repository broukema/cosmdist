/*
   cosmdist - standard FLRW cosmological distance functions

   Copyright (C) 2004-2019 Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

*/

#include <errno.h>
#include "cosmdist.h"

/* This is the integrand to obtain a comoving (also called proper)
   distance, derived from the Friedmann equation.
   The c/H_0  constant is not included here, since it is a constant.
   See e.g. http://en.wikipedia.org/wiki/Comoving_distance  */

double cosm_dist_integrand (double a, void * p) {
  struct cosm_struct_local_params * params = (struct cosm_struct_local_params *)p;

  double omm = (params->omm);
  double omlam = (params->omlam);
  double omrad = (params->omrad);
  double w_0 = (params->w_0);
  double f;

  /*  printf("::: a = %.4f omm= %.4f \n", a,omm); */

  f=
    1.0 /
    ( a
      *sqrt( omm/a
             + omrad/(a*a)
             - ( omm +omlam +omrad -1.0 )
             + omlam*pow(a, -(1.0+ 3.0* w_0))
             )
      );

  return f;
}


/* This is the integrand to obtain the cosmological time from a given
   redshift to the present, derived from the Friedmann equation.
   The c/H_0  constant is not included here, since it is a constant.
   See e.g. http://en.wikipedia.org/wiki/Comoving_distance  */

double cosm_time_integrand (double a, void * p) {
  struct cosm_struct_local_params * params = (struct cosm_struct_local_params *)p;

  double omm = (params->omm);
  double omlam = (params->omlam);
  double omrad = (params->omrad);
  double w_0 = (params->w_0);
  double f;

  /*  printf("::: a = %.4f omm= %.4f \n", a,omm); */

  f=
    1.0 /
    (sqrt( omm/a
           + omrad/(a*a)
           - ( omm +omlam +omrad -1.0 )
           + omlam*pow(a, -(1.0+ 3.0* w_0))
           )
     );

  return f;
}



double cosm_da_dt (double H_0,
                   double omm,
                   double omlam,
                   double omrad,
                   double w_0,
                   double a
                   ){
  double f;

  double w_a = 0.5;
  double w;
  double omlam_effective;

  w= w_0 + w_a * (1-a);  /* Clemson & Liddle 2009 arXiv:0811.4676 */
  w= w_0;

  /* conventional one-parameter quintessence */
  omlam_effective = omlam*pow(a, -(1.0+ 3.0* w)) ;

  /* if(a>0.3){ omlam_effective = 0.0; };  */


  /*  printf("::: a = %.4f omm= %.4f \n", a,omm); */

  f= H_0 *
    (sqrt( omm/a
           + omrad/(a*a)
           - ( omm +omlam +omrad -1.0 )
           + omlam_effective
           )
     );

  return f;
};



int
cosm_dist_init (  struct cosm_param_chain_element *p_chain )
{

  gsl_integration_workspace * work
    = gsl_integration_workspace_alloc (COSM_MAX_INTEGRATION_INTERVALS);
  gsl_integration_workspace * work_time
    = gsl_integration_workspace_alloc (COSM_MAX_INTEGRATION_INTERVALS);

  /* declarations */
  int want_debug= COSM_WANT_DEBUG ;  /* local to this function */

  double result, error;
  /*
      double omm = 1.0;
      double omlam = 0.0;
      double w_0 = -1.0;
  */
  double omcurv ;

  double z2 = 1e6 ;  /* !! Warning: could be important in long term !!  */
  double a2 ;
  double a0 = 1.0;  /*  at observer */
  double a1 = 1.05;  /* in future, in order for integral to be smooth through a0 */

  double offset_nounits ; /* distance integral from  a0 to a1 */

  /* double a3 = 0.30, d3 =-99.9; */  /* scale factor a3  and  distance for debugging */



  /*  spline points */
  /* a increasing */
  double aa[SIZE_OF_COSM_SPLINE_TABLE];   /* scale factor intervals */
  double dd[SIZE_OF_COSM_SPLINE_TABLE];   /* distance values */
  double tt[SIZE_OF_COSM_SPLINE_TABLE];   /* time values */

  /* a decreasing */
  double aa_dec[SIZE_OF_COSM_SPLINE_TABLE];   /* scale factor intervals */
  double dd_dec[SIZE_OF_COSM_SPLINE_TABLE];   /* distance values */
  double tt_dec[SIZE_OF_COSM_SPLINE_TABLE];   /* time values */

  int   i_integrate;    /* iteration through integrals */
  double delta_a;
  double horizon;  /* local copy of comoving horizon distance */
  /* double pm_max; */ /* local copy of maximum comoving tangential distance (pm dist) */
  double t_0;    /* local copy of present age of universe */

  gsl_function F;  /* comoving distance */
  gsl_function F_time;  /* cosmological time */
  struct cosm_struct_local_params params_copy;  /* copy local to this function */


  /* statements */

  if(want_debug){
    printf("first statement in cosm_dist_init\n");
  };


  /* checks on input values */
  if( (p_chain->params).H_0 < COSM_H_0_TOLERANCE ){
    errno = EDOM; /* static FLRW not handled */
    printf("cosmdist_init: ERROR: H_0=%.2g < %.2g ; static FLRW models are out of scope.\n",(p_chain->params).H_0, COSM_H_0_TOLERANCE);
    exit (errno);
  };


  a2 = 1.0/(1.0+ z2);

  params_copy.omm = (p_chain->params).omm;
  params_copy.omlam= (p_chain->params).omlam;
  params_copy.omrad= (p_chain->params).omrad;
  params_copy.w_0= (p_chain->params).w_0;


  if(want_debug){
    printf("cosm_dist_init:  omm = %.4f\n", params_copy.omm);
    printf("cosm_dist_init:  omlam = %.4f\n", params_copy.omlam);
    printf("cosm_dist_init:  omrad = %.4f\n", params_copy.omrad);
    printf("cosm_dist_init:  w_0 = %.4f\n", params_copy.w_0);
  };

  /*  radius of curvature */
  omcurv= params_copy.omm + params_copy.omlam + params_copy.omrad - 1.0;

  if(want_debug)
    {
      printf("cosm_dist_init: omcurv= %.4f\n",omcurv);
      printf("fabs(omcurv)= %.4f  COSM_CURV_TOLERANCE = %.6f\n",
             fabs(omcurv), COSM_CURV_TOLERANCE);
    };

  if (fabs(omcurv) < COSM_CURV_TOLERANCE )
    {
      if(want_debug)
        {
          printf("cosm_dist_init: flat case\n");
        };

      (p_chain->params).k_curv = 0;
    }
  else if (omcurv >= COSM_CURV_TOLERANCE )
    {
      if(want_debug)
        {
          printf("cosm_dist_init: spherical case\n");
        };

      (p_chain->params).k_curv = 1;
      (p_chain->params).R_C = COSM_C_ON_H_0_MPC
        / (0.01* (p_chain->params).H_0 *sqrt(omcurv));
    }
  else  /* if (omcurv <= COSM_CURV_TOLERANCE) */
    {
      if(want_debug)
        {
          printf("cosm_dist_init: hyperbolic case\n");
        };

      (p_chain->params).k_curv = -1;
      (p_chain->params).R_C = COSM_C_ON_H_0_MPC
        / (0.01* (p_chain->params).H_0 *sqrt(-omcurv));
    };

  if(want_debug){
    printf("cosm_dist_init:  k_curv = %d  R_C = %.2f\n",
           (p_chain->params).k_curv,(p_chain->params).R_C);
  };


  /* comov distance */
  F.function = &cosm_dist_integrand;
  F.params = &params_copy;

  /* cosm time */
  F_time.function = &cosm_time_integrand;
  F_time.params = &params_copy;

  /*
    if(want_debug)
    {
      printf("will integrate from a= %.4f to a= %.4f \n",a1,a2);
    };
  */

  delta_a=  (a2-a1)/( (double) SIZE_OF_COSM_SPLINE_TABLE);

  if(want_debug){
    printf("\n");
  };


  /* offset from now to future; so that integration is smooth through now */
  gsl_integration_qag (&F, a0, a1,
                       COSM_INTEGRATION_ABS_PRECISION,
                       COSM_INTEGRATION_PRECISION,
                       COSM_MAX_INTEGRATION_INTERVALS,
                       COSM_QAG_KEY,
                       work, &offset_nounits, &error);

  /* horizon distance */
  gsl_integration_qag (&F, a2, a0,
                       COSM_INTEGRATION_ABS_PRECISION,
                       COSM_INTEGRATION_PRECISION,
                       COSM_MAX_INTEGRATION_INTERVALS,
                       COSM_QAG_KEY,
                       work, &horizon, &error);

  (p_chain->params).horizon= horizon * COSM_C_ON_H_0_MPC
      /( 0.01 * (p_chain->params).H_0) ;

  /* maximum proper motion (\equiv tangential comoving distance) */
  if((p_chain->params).k_curv == 0)
    {
      (p_chain->params).pm_max = (p_chain->params).horizon;
    }
  else if((p_chain->params).k_curv == -1)
    {
      (p_chain->params).pm_max = (p_chain->params).R_C *
        sinh( ( (p_chain->params).horizon / (p_chain->params).R_C ) );
    }
  else if((p_chain->params).k_curv == 1)
    {
      (p_chain->params).pm_max = (p_chain->params).R_C;
    }
  else
    printf("cosmdist_init.c: There is a coding error in this function.");

  /* maximum time = age of universe */

  gsl_integration_qag (&F_time, a2, a0,
                         COSM_INTEGRATION_ABS_PRECISION,
                         COSM_INTEGRATION_PRECISION,
                         COSM_MAX_INTEGRATION_INTERVALS,
                         COSM_QAG_KEY,
                         work_time, &t_0, &error);

  (p_chain->params).t_0= t_0
      /( COSM_H_0_INV_GYR * (p_chain->params).H_0) ;


  /* main integration loop */

  for (i_integrate= 0; i_integrate < SIZE_OF_COSM_SPLINE_TABLE;
       i_integrate++){
    /*
      a  decreasing
      aa[i_integrate]=  a1 + ((double)i_integrate+0.5) * delta_a;
    */

    /* a  increasing */
    aa[i_integrate]=  a2 - ((double)i_integrate+0.5) * delta_a;

    /*  distance integral */
    gsl_integration_qag (&F, aa[i_integrate], a1,
                         COSM_INTEGRATION_ABS_PRECISION,
                         COSM_INTEGRATION_PRECISION,
                         COSM_MAX_INTEGRATION_INTERVALS,
                         COSM_QAG_KEY,
                         work, &result, &error);

    /* normalise with c/H_0 */
    dd[i_integrate]= (result-offset_nounits)   * COSM_C_ON_H_0_MPC
      /( 0.01 * (p_chain->params).H_0) ;

    /* time integral */
    /* a2 must be very close to zero to have a precise time  */
    gsl_integration_qag (&F_time, a2, aa[i_integrate],
                         COSM_INTEGRATION_ABS_PRECISION,
                         COSM_INTEGRATION_PRECISION,
                         COSM_MAX_INTEGRATION_INTERVALS,
                         COSM_QAG_KEY,
                         work_time, &result, &error);

    tt[i_integrate]= result
      /( COSM_H_0_INV_GYR * (p_chain->params).H_0) ;

    /* a decreasing */
    aa_dec[ SIZE_OF_COSM_SPLINE_TABLE - i_integrate -1 ]
      = aa[i_integrate];
    dd_dec[ SIZE_OF_COSM_SPLINE_TABLE - i_integrate -1 ]
      = dd[i_integrate];
    tt_dec[ SIZE_OF_COSM_SPLINE_TABLE - i_integrate -1 ]
      = tt[i_integrate];


    if(want_debug && i_integrate < 10)
      {
        printf("doing integration... i_integrate = %d",i_integrate);
        printf("\naa[i_integrate] = %.4f dd[i_integrate] = %.4f \n",
               aa[i_integrate], dd[i_integrate]);
      }
    else
      {
        if(want_debug && i_integrate == 10)
          {
            printf("... more than 10 intervals...\n");
          };
      };

  };   /*   for (i_integrate= 0; i_integrate < SIZE_OF_COSM_SPLINE_TABLE;  */


  /* free up memory allocated for work space */
  gsl_integration_workspace_free (work);
  gsl_integration_workspace_free (work_time);

  /*  comoving distance = fn(z) spline  */
  p_chain->spline_accel_comov =
    gsl_interp_accel_alloc  ();
  p_chain->spline_object_comov =
    gsl_spline_alloc( gsl_interp_cspline, SIZE_OF_COSM_SPLINE_TABLE);

  gsl_spline_init( p_chain->spline_object_comov, aa, dd,
                   SIZE_OF_COSM_SPLINE_TABLE);

  /*  z = fn(comoving distance) spline  */
  p_chain->spline_accel_comov_inv =
    gsl_interp_accel_alloc  ();
  p_chain->spline_object_comov_inv =
    gsl_spline_alloc( gsl_interp_cspline, SIZE_OF_COSM_SPLINE_TABLE);

  gsl_spline_init( p_chain->spline_object_comov_inv, dd_dec, aa_dec,
                   SIZE_OF_COSM_SPLINE_TABLE);


  /*  cosm time = fn(z) spline  */
  p_chain->spline_accel_time =
    gsl_interp_accel_alloc  ();
  p_chain->spline_object_time =
    gsl_spline_alloc( gsl_interp_cspline, SIZE_OF_COSM_SPLINE_TABLE);

  gsl_spline_init( p_chain->spline_object_time, aa, tt,
                   SIZE_OF_COSM_SPLINE_TABLE);

  /*  z = fn(cosm time) spline  */
  p_chain->spline_accel_time_inv =
    gsl_interp_accel_alloc  ();
  p_chain->spline_object_time_inv =
    gsl_spline_alloc( gsl_interp_cspline, SIZE_OF_COSM_SPLINE_TABLE);

  gsl_spline_init( p_chain->spline_object_time_inv, tt, aa,
                   SIZE_OF_COSM_SPLINE_TABLE);


  if(want_debug)
    {
      /*
        printf("cosmdist_init: after  gsl_spline_init ");
      */

      /*
        d3=
        gsl_spline_eval( p_chain->spline_object_comov,
        a3, p_chain->spline_accel_comov );
        printf("distance to a = %.4f is  %.2f  Mpc \n", a3, d3);
      */

    };



  /*
      gsl_integration_qags (&F, a1, a2, 1.0, 1e-5, 10000,
      work, &result, &error);
      result = result * 300000.0/100.0;

      if(want_debug)
      {
      printf("omm = %.2f  omlam = %.2f  w_0 = %.2f\n", omm, omlam, w_0);
      printf("distance to z = %.4f is  %.2f  Mpc \n", z1, result);
      printf("absolute error = %.2f Mpc\n", error);
      printf("number of intervals used = %d\n", (*work).size);
      };
  */


  return 0;
}
