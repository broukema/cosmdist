/* 
   zmin_alpha - get minimum redshift and Omega_Lambda for Poincare model

   Copyright (C) 2012 Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

   See also http://www.gnu.org/licenses/gpl.html

*/


/*  DESCRIPTION: Depending on the matched circle size *alpha* for a
    Poincare dodecahedral space (FLRW metric) model of the Universe,
    and given a fixed input value of either the matter density
    parameter *omm* or the dark energy parameter *omlam*, first
    calculate the other of these two parameters by successive
    approximation so that *alpha* corresponds to a fundamental domain
    size of 2\pi/10 in units of the curvature radius *rc_best*, and
    secondly calculate the redshift at the centre of opposite matched
    faces, i.e. the minimum redshift for seeing opposite images at an
    identical cosmological epoch. The first step results in calculating
    *rSLS_best* and *rc_best* in addition to *omm* or *omlam*.

    TODO:
    low priority, but possible: generalise to other k > 0 models
*/

#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <stdlib.h>

#define EXIT_FAILURE 1

#if ENABLE_NLS
# include <libintl.h>
# define _(Text) gettext (Text)
#else
# define textdomain(Domain)
# define _(Text) Text
#endif
#define N_(Text) Text

#include <math.h>
#include "cosmdist.h"

double cosm_get_zmin_from_alpha
( double H_0, /* input */
  double *omm,  /* input if vary_lambda; otherwise output */
  double *omlam, /* output if vary_lambda; otherwise input */
  int    vary_lambda, /* input: vary omlam? 0/1 = no/yes */
  double omrad,  /* input */
  double w_0,  /* input */
  double alpha, /* input */
  double *rSLS_best, /* output */
  double *rc_best, /* output */
  double *z_inverse /* output */
  )
{

  /* if vary_lambda==1 */
  double omlam_1 ;
  double omlam_2 = 0.8 ;
  int  N_omlam = 1000; /* 100; */
  int  i_omlam;

  /* if vary_lambda==0 */
  double omm_1 ;
  double omm_2 = 1.3 ;
  int  N_omm = 1000; /* 100; */
  int  i_omm;

  const double zSLS = 1100;
  const double c_kms = 299792.458; 
  double rc_direct, rSLS, rc_from_alpha;
  double omlam_best, omm_best ;
  double rc_diff = 9e9; 
  double rc_diff_1 = 9e9, rc_diff_2;
  double rc_error;
  double min_rc_error = 9e9; /* minimum abs error between two ways of estimating R_C */
  double required_rc_error = 1.0; /* in Mpc */


  int want_debug = (0 || COSM_WANT_DEBUG);

  /*  REAL WORK
      default: find best *omlam given *omm, etc., alpha 
      See Eq.~(15) \citep{RBSG08}  \tan \frac{\rSLS}{R_C} = \frac{\tan (\pi/10)}{\cos \alpha}
  */

  if(vary_lambda){
    /* del_omlam = (omlam_2-omlam_1)/(double)N_omlam; */
    omlam_1 = 1.0 - (*omm) + 1e-4;
  }else{
    omm_1 = 1.0 - (*omlam) +1e-4;
  };
  

  /* TODO: this code should be generalised, not repeated with a minor variation */
  if(vary_lambda){

    /*  for(i_omlam=0;i_omlam<N_omlam;i_omlam++){ */
    i_omlam=0;
    *omlam=9e9;
    while(i_omlam<N_omlam && min_rc_error > required_rc_error){
      /* omlam = omlam_1 + ((double)(i_omlam)+0.5)*del_omlam; */
      switch(i_omlam){
      case 0: 
        (*omlam) = omlam_1;
        if(want_debug)printf("AA.0: %d %.3f %.3f\n",i_omlam, (*omm) , (*omlam));
        break;
      case 1:
        (*omlam) = omlam_2;
        if(want_debug)printf("AA.1: %d %.3f %.3f\n",i_omlam, (*omm) , (*omlam));
        break;
      default:
        (*omlam) = 0.5 * (omlam_1 + omlam_2);
        if(want_debug)printf("AA.other: %d %.3f %.3f\n",i_omlam, (*omm) , (*omlam));
      };

      if(want_debug)printf("AA: %d %.3f %.3f\n",i_omlam, (*omm) , (*omlam));
      if((*omm) + (*omlam) -1.0 > 0.0){
        rc_direct= c_kms /H_0 / sqrt((*omm) + (*omlam) -1.0); /* use metric parameters directly */
      
        errno=0;
        rSLS = cosm_get_comov_dist( H_0, (*omm), (*omlam), omrad, w_0, zSLS );
        if(errno != 0){
          perror("ERROR during cosm_get_comov_dist");
        }else{
          rc_from_alpha = rSLS / atan( tan(M_PI/10.0)/cos(alpha*M_PI/180.0) ) ;
          rc_diff = rc_direct - rc_from_alpha ;
          rc_error = fabs( rc_diff );
          if(want_debug) printf(" new: %.1f %.1f %.1f, %.5f %.5f %.5f ; ",
                                rc_diff_1, rc_diff, rc_diff_2, omlam_1, (*omlam), omlam_2);
          if(rc_error < min_rc_error ){
            /* update best estimate parameters */
            min_rc_error  = rc_error ;  
            omlam_best = (*omlam);
            *rSLS_best = rSLS ;
            *rc_best = 0.5* (rc_direct+rc_from_alpha);
            if(want_debug) printf("better: ");
          } /* if(fabs( rc_direct - rc_from_alpha ) < min_rc_error ) */
          else {
            if(want_debug) printf("worse:  ");
          };

          /* tighten domain except at first two steps */
          if(i_omlam >= 2){ /* iterative improvement */
            if( (rc_diff / rc_diff_1) > 0.0){
              omlam_1 = (*omlam);
              rc_diff_1 = rc_diff;
            }else {
              omlam_2 = (*omlam);
              rc_diff_2 = rc_diff;
            };
          } else { /* initialise diffs if at first two steps */
            if(i_omlam==0){
              rc_diff_1 = rc_diff;
            }else{
              rc_diff_2 = rc_diff;
            };
          };

          if(want_debug){
            printf("i= %d  (*omm)= %.5f  (*omlam)= %.5f ",i_omlam,(*omm),(*omlam));
            printf("rc_direct= %.2f  rc_from_alpha= %.2f  rc_error= %.2f rSLS= %.2f\n",
                   rc_direct, rc_from_alpha, rc_error,rSLS );
          };
        }; /* if distance found without error */
        if(want_debug) printf(" DDD %d\n",i_omlam);
      } /* if((*omm) + (*omlam) -1.0 > 0.0) */
      else { /* in this case, need to increase (*omlam) */
        omlam_1 = (*omlam);
        rc_diff_1 = rc_diff;
      };
      i_omlam++;
      if(want_debug)printf("\n");
    }; /*  while( )  || for(i_omlam=0;i_omlam<N_omlam;i_omlam++) */
  }else{
    /*  for(i_omlam=0;i_omlam<N_omlam;i_omlam++){ */
    i_omm=0;
    (*omm)=9e9;
    while(i_omm<N_omm && min_rc_error > required_rc_error){
      /* (*omlam) = omlam_1 + ((double)(i_omlam)+0.5)*del_omlam; */
      switch(i_omm){
      case 0: 
        (*omm) = omm_1;
        if(want_debug)printf("AA.0: %d %.3f %.3f\n",i_omm, (*omm) , (*omlam));
        break;
      case 1:
        (*omm) = omm_2;
        if(want_debug)printf("AA.1: %d %.3f %.3f\n",i_omm, (*omm) , (*omlam));
        break;
      default:
        (*omm) = 0.5 * (omm_1 + omm_2);
        if(want_debug)printf("AA.other: %d %.3f %.3f\n",i_omm, (*omm) , (*omlam));
      };

      if(want_debug)printf("AA: %d %.3f %.3f\n",i_omm, (*omm) , (*omlam));
      if((*omm) + (*omlam) -1.0 > 0.0){
        rc_direct= c_kms /H_0 / sqrt((*omm) + (*omlam) -1.0); /* use metric parameters directly */
      
        errno=0;
        rSLS = cosm_get_comov_dist( H_0, (*omm), (*omlam), omrad, w_0, zSLS );
        if(errno != 0){
          perror("ERROR during cosm_get_comov_dist");
        }else{
          rc_from_alpha = rSLS / atan( tan(M_PI/10.0)/cos(alpha*M_PI/180.0) ) ;
          rc_diff = rc_direct - rc_from_alpha ;
          rc_error = fabs( rc_diff );
          if(want_debug) printf(" new: %.1f %.1f %.1f, %.5f %.5f %.5f ; ",
                                rc_diff_1, rc_diff, rc_diff_2, 
                                omm_1, (*omm), omm_2);
          if(rc_error < min_rc_error ){
            /* update best estimate parameters */
            min_rc_error  = rc_error ;  
            omm_best = (*omm);
            *rSLS_best = rSLS ;
            *rc_best = 0.5* (rc_direct+rc_from_alpha);
            if(want_debug) printf("better: ");
          } /* if(fabs( rc_direct - rc_from_alpha ) < min_rc_error ) */
          else {
            if(want_debug) printf("worse:  ");
          };

          /* tighten domain except at first two steps */
          if(i_omm >= 2){ /* iterative improvement */
            if( (rc_diff / rc_diff_1) > 0.0){
              omm_1 = (*omm);
              rc_diff_1 = rc_diff;
            }else {
              omm_2 = (*omm);
              rc_diff_2 = rc_diff;
            };
          } else { /* initialise diffs if at first two steps */
            if(i_omm==0){
              rc_diff_1 = rc_diff;
            }else{
              rc_diff_2 = rc_diff;
            };
          };

          if(want_debug){
            printf("i= %d  (*omm)= %.5f  (*omlam)= %.5f ",i_omm,(*omm),(*omlam));
            printf("rc_direct= %.2f  rc_from_alpha= %.2f  rc_error= %.2f rSLS= %.2f\n",
                   rc_direct, rc_from_alpha, rc_error,rSLS );
          };
        }; /* if distance found without error */
        if(want_debug) printf(" DDD %d\n",i_omm);
      } /* if((*omm) + (*omlam) -1.0 > 0.0) */
      else { /* in this case, need to increase (*omlam) */
        omm_1 = (*omm);
        rc_diff_1 = rc_diff;
      };
      i_omm++;
      if(want_debug)printf("\n");
    }; /*  while( )  || for(i_omlam=0;i_omlam<N_omlam;i_omlam++) */
  }; /* if(vary_lambda) */


  if(vary_lambda){
    if(want_debug)
      printf("done: %d %d  %.1f %.1f\n",
             i_omlam, N_omlam, min_rc_error, required_rc_error);
  }else{
    if(want_debug)
      printf("done: %d %d  %.1f %.1f\n",
             i_omm, N_omm, min_rc_error, required_rc_error);
  };


  /* now evaluate pi/10 R_C  and get the corresponding redshift to the matched disc centre */
  errno=0;
  if(vary_lambda){
    *z_inverse= cosm_get_comov_dist_inv(H_0, (*omm), (*omlam), omrad, w_0, 
                                       (M_PI/10.0 * (*rc_best))
                                      ); 
  }else{
    *z_inverse= cosm_get_comov_dist_inv(H_0, (*omm), (*omlam), omrad, w_0, 
                                       (M_PI/10.0 * (*rc_best))
                                      ); 
  };

  if(errno != 0)
    perror("ERROR during cosm_get_comov_dist_inv");

  cosm_dist_free();
  return 0;
};
