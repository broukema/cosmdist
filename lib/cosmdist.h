/*
   cosmdist - standard FLRW cosmological distance functions

   Copyright (C) 2004-2019 Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

*/


/* declarations for cosmological distance (and time) functions
   See e.g. the GFDL open encyclopedia entry
   http://en.wikipedia.org/wiki/Comoving_distance  */

#include <stdio.h>
#include <math.h>
#include <gsl/gsl_integration.h>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>

#include <stddef.h>
#include <errno.h>
#include <string.h>

/* #include "config.h" */ /* hack boud 5.11.2011 */
/* Define to a macro mangling the given C identifier (in lower and upper
   case), which must not contain underscores, for linking with Fortran. */
#define FC_FUNC(name,NAME) name ## _
/* As FC_FUNC, but for C identifiers containing underscores. */
#define FC_FUNC_(name,NAME) name ## _


#define COSM_WANT_DEBUG 0


/*
   USER PARAMETERS - BEGIN
   These are parameters for the GSL integration routine - the user can
   probably modify these without leading to bugs.
*/

#define SIZE_OF_COSM_SPLINE_TABLE 30000 /* number of values, not number of bytes */
#define COSM_INTEGRATION_ABS_PRECISION 0.001
#define COSM_INTEGRATION_PRECISION 1e-8
#define COSM_MAX_INTEGRATION_INTERVALS 300000
#define COSM_QAG_KEY 6


/* tolerance for deciding if same parameter */
#define COSM_SAME_TOLERANCE 1e-5

/* tolerance for deciding if redshift < 0, ... */
#define COSM_TOLERANCE 1e-5

/* tolerance for deciding if curvature is non-zero */
#define COSM_CURV_TOLERANCE 1e-5

/* tolerance for deciding if too close to cosmic equator (k > 0) */
#define COSM_EQUATOR_TOLERANCE 1e-5

/* tolerance for deciding if H_0 is too low */
#define COSM_H_0_TOLERANCE 1e-20

#define MAX_COSM_PARAMS 1000  /* max no of sets of gsl splines to save */

/*
   USER PARAMETERS - END
   You probably should not change parameters below unless you're really
   sure you know what you're doing.
*/

/* fundamental constants useful in cosmology */

/*  c/H_0  expressed in units of h^{-1} Mpc, where h = H_0/(100 km/s/Mpc)
    In other words, it's   c  in km/s  divided by 100 km/s/Mpc  .
 */
#define COSM_C_ON_H_0_MPC  2997.92458

/*
       COSM_H_0_INV_GYR = 1 kms^-1 Mpc^-1 * 3.15576e16 s Gyr^-1
       = 10e3 m s^-1 (3.08568e22 m)^-1  * 3.15576e16 s Gyr^-1
       = 0.00102271 Gyr^-1

       This converts conventional H_0 units to inverse Gigayears.
*/
#define COSM_H_0_INV_GYR   1.02271e-3


/* #define SIZE_OF_COSM_STRUCT_LOCAL_PARAMS 144 */ /* enough for 128bit machines ;) */
#define SIZE_OF_COSM_STRUCT_LOCAL_PARAMS (sizeof(struct cosm_struct_local_params))
struct cosm_struct_local_params {
  double H_0; /*  Hubble constant in units of 100km/s/Mpc */
  double omm; /* Omega_matter */
  double omlam; /* Omega_cosmological_constant or Omega_quintessence */
  double omrad; /* Omega_radiation (photons) */
  double w_0; /* zeroth derivative in  w  quintessence parameter */
  int    k_curv;  /*  -1, 0 or 1 */
  double R_C;  /* radius of curvature  - intialised in cosm_check_initialisation */
  double horizon;  /* horizon radius - intialised in cosm_check_initialisation */
  double pm_max; /* max pm distance - intialised in cosm_check_initialisation */
  double t_0;    /* age of universe - intialised in cosm_check_initialisation */
};




/*
   Enough space for 128bit machines ;)
   This does not include the cosm_struct_local_params space - you must add it:
   e.g.
   p_chain= malloc( SIZE_OF_COSM_PARAM_CHAIN_ELEMENT +
                     SIZE_OF_COSM_STRUCT_LOCAL_PARAMS );
*/

/* #define SIZE_OF_COSM_PARAM_CHAIN_ELEMENT 144 */
#define SIZE_OF_COSM_PARAM_CHAIN_ELEMENT (sizeof(struct cosm_param_chain_element))

struct cosm_param_chain_element {
  /*  pointer to next element in chain - NULL if final */
  struct cosm_param_chain_element  *next;

  /* useful content of this element */
  struct cosm_struct_local_params  params; /* parameter record */

  /* comoving distance as function of redshift */
  gsl_spline *spline_object_comov;       /* gsl spline interpolation object */
  gsl_interp_accel *spline_accel_comov;  /* accelerator for gsl spline */
  /* inverse of: comoving distance as function of redshift  */
  gsl_spline *spline_object_comov_inv;       /* gsl spline interpolation object */
  gsl_interp_accel *spline_accel_comov_inv;  /* accelerator for gsl spline */

  /* time as function of redshift */
  gsl_spline *spline_object_time;       /* gsl spline interpolation object */
  gsl_interp_accel *spline_accel_time;  /* accelerator for gsl spline */
  /* inverse of: time as function of redshift  */
  gsl_spline *spline_object_time_inv;       /* gsl spline interpolation object */
  gsl_interp_accel *spline_accel_time_inv;  /* accelerator for gsl spline */


};

/* *p_first, *p_last were here... in 0.1.6 and earlier */



/* SUGGESTION:  M-% p_first  cosm_p_first  etc  */



/*
   Function to find out if the set of input local cosmological
   parameters has already been used to initialise the time and
   distance integrals.
*/

int cosm_get_params_index( struct cosm_struct_local_params p_input,
                           struct cosm_param_chain_element ** p_p_chain);



double cosm_get_comov_dist( double H_0, double omm, double omlam,
                            double omrad,  double w_0,
                            double redshift
                            );

double cosm_get_comov_dist_inv( double H_0, double omm, double omlam,
                            double omrad,  double w_0,
                            double distance
                            );

double cosm_get_comov_time( double H_0, double omm, double omlam,
                            double omrad,  double w_0,
                            double redshift
                            );

double cosm_get_comov_time_inv( double H_0, double omm, double omlam,
                            double omrad,  double w_0,
                            double time
                            );


double cosm_get_pm_dist( double H_0, double omm, double omlam,
                            double omrad,  double w_0,
                            double redshift
                            );

double cosm_get_pm_dist_inv(
                            double H_0, double omm,
                            double omlam,
                            double omrad,  double w_0,
                            double distance
                            );

double cosm_get_R_C( double H_0, double omm, double omlam,
                            double omrad,  double w_0
                            );

double cosm_get_horizon( double H_0, double omm, double omlam,
                            double omrad,  double w_0
                            );


double cosm_da_dt( double H_0, double omm, double omlam,
                   double omrad,  double w_0,
                   double a
                   );


int curv_evolution(double H_0, double omm, double omlam,
                   double omrad,  double w_0
                   );


int cosm_check_initialisation(
                              struct cosm_struct_local_params p_input,
                              struct cosm_param_chain_element ** p_p_chain
                              );

int cosm_dist_free(void);

int
cosm_dist_init (  struct cosm_param_chain_element *p_chain );


/* Ue-Li Pen fitting function */
double cosm_eta_Ue_Li_Pen(double redshift, double omm);

double cosm_get_pm_Ue_Li_Pen( double H_0, double omm,
                              double redshift
                              );


double cosm_get_zmin_from_alpha
( double H_0, /* input */
  double *omm,  /* input if vary_lambda; otherwise output */
  double *omlam, /* output if vary_lambda; otherwise input */
  int    vary_lambda, /* input: vary omlam? 0/1 = no/yes */
  double omrad,  /* input */
  double w_0,  /* input */
  double alpha, /* input */
  double *rSLS_best, /* output */
  double *rc_best, /* output */
  double *redshift /* output */
  );


#define TESTF FC_FUNC(testf,TESTF)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
void TESTF(
           double  *parameter_input,
           double *H_0, double *omm,
           double *omlam,
           double *omrad, double *w_0,
           int *want_all,
           int *want_verbose,
           int *want_pm,
           int *want_inverse,
           int *want_time,
           int *want_horizon,
           int *want_curvature
           );


/* comoving distance */

#define DOFZ FC_FUNC(dofz,DOFZ)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
double DOFZ(
            double *H_0, double *omm, double *omlam,
            double *omrad, double *w_0,
            double *redshift
            );

#define ZOFD FC_FUNC(zofd,ZOFD)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
double ZOFD(
            double *H_0, double *omm, double *omlam,
            double *omrad, double *w_0,
            double *distance
            );

/* proper motion distance */

#define DPMOFZ FC_FUNC(dpmofz,DPMOFZ)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
double DPMOFZ(
              double *H_0, double *omm, double *omlam,
              double *omrad, double *w_0,
              double *redshift
            );

#define ZOFDPM FC_FUNC(zofdpm,ZOFDPM)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
double ZOFDPM(
              double *H_0, double *omm, double *omlam,
              double *omrad, double *w_0,
              double *distance
            );

/* cosmological time */

#define TOFZ FC_FUNC(tofz,TOFZ)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
double TOFZ(
            double *H_0, double *omm, double *omlam,
            double *omrad, double *w_0,
            double *redshift
            );

#define ZOFT FC_FUNC(zoft,ZOFT)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
double ZOFT(
            double *H_0, double *omm, double *omlam,
            double *omrad, double *w_0,
            double *time
            );


#define RCURV FC_FUNC(rcurv,RCURV)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
double RCURV(
            double *H_0, double *omm, double *omlam,
            double *omrad, double *w_0
            );

#define RHORIZ FC_FUNC(rhoriz,RHORIZ)
#ifdef __cplusplus
extern "C"  /* prevent C++ name mangling */
#endif
double RHORIZ(
            double *H_0, double *omm, double *omlam,
            double *omrad, double *w_0
            );
