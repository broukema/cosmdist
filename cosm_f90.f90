!   cosmdist - standard FLRW cosmological distance functions
!
!   Copyright (C) 2004 Boud Roukema
!
!   This program is free software; you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation; either version 2, or (at your option)
!   any later version.
!
!   This program is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with this program; if not, write to the Free Software Foundation,
!   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

      subroutine i2log( i, logic)
      integer    i
      logical*4  logic
      if(i.eq.1)then
         logic=.true.
      else
         logic=.false.
      endif
      return
      end

      


      subroutine testf( par_input, H_0, omm, omlam, omrad, w_0, &
           iw_all,iw_verb,iw_pm,iw_inverse,iw_time,  &
           iw_hor,iw_curv)

      ! See https://stackoverflow.com/questions/838310/fortran-90-kind-parameter
      ! for more modern fortran type declarations.
        
      implicit real*8 (a-h,o-z)
      integer  ierrorflag

      logical*4 w_all,w_verb,w_pm,w_inverse,w_time,w_hor,w_curv

      if(iw_verb.gt.0)then
         print*,'testf: You may look at the source code  cosm_f90.f90'
         print*,'to see an example of how to call the cosmdist '
         print*,'routines from a fortran program.  Of course, you'
         print*,'also need to link your fortran routine with the'
         print*,'compiled library:   libcosmdist.a  '
         print*,'You should declare these functions as real*8:'
         print*,'      real*8  dofz,zofd,dpmofz,zofdpm,dpmofz,zofpdm'
         print*,'      real*8  tofz,zoft,rcurv,rhoriz'
         print*,'and make any needed type conversions (to or from'
         print*,'real*4) explicitly.'
         print*,' '

         write(6,'(a,a,6f8.2,a,5i2)')'par_input, H_0, omm, omlam, omrad, w_0,', &
              'iw_pm,iw_inverse,iw_time,iw_hor,iw_curv=', &
              par_input, H_0, omm, omlam, omrad, w_0, &
              '   ', &
              iw_pm,iw_inverse,iw_time,iw_hor,iw_curv
      endif

      ! convert from C integer to fortran logical
      call i2log(iw_all,w_all)
      call i2log(iw_verb,w_verb)
      call i2log(iw_pm,w_pm)
      call i2log(iw_inverse,w_inverse)
      call i2log(iw_time,w_time)
      call i2log(iw_hor,w_hor)
      call i2log(iw_curv,w_curv)

!      x= dofz(H_0,omm,omlam,w_0, par_input)
!      print*,'testf: x=',x

      if(w_inverse)then
         if(.not.(w_time))then
            distance= par_input
         else
            time= par_input
         endif
      else
         redshift= par_input
      endif


!     /*  comoving distance and inverse */

      if(w_all .or. ((.not.(w_pm)) .and. (.not.(w_inverse)) & !
           .and. (.not.(w_time))))then
         dist= dofz( H_0, omm, omlam, omrad, w_0, redshift)
         if(w_verb)then
            write(6,'(a,f10.2,a)')'comoving distance is', dist, ' Mpc'
         else
            write(6,'(f10.2,$)')dist
         endif
      endif
      
      if((w_all) .or. ((.not.(w_pm)) .and. (w_inverse) & !
           .and. (.not.(w_time))))then
         if((w_all)) distance=dist
         red_inv=  & !
              zofd( H_0, omm, omlam, omrad, w_0, distance )
         if(w_verb)then
            write(6,'(a,f9.4,a)')'redshift is',red_inv
         else
            write(6,'(f9.4,$)')red_inv
         endif
      endif
      
      
!     /* proper motion distance and inverse */

      if((w_all) .or. ((w_pm) .and. (.not.(w_inverse))  & !
       .and. (.not.(w_time))))then
         dist= dpmofz( H_0, omm, omlam, omrad, w_0, redshift )
         if(w_verb)then
            write(6,'(a,f10.2,a)')'proper motion distance is',dist, ' Mpc'
         else
            write(6,'(f10.2,$)')dist
         endif
      endif
      
      
      if((w_all) .or. ((w_pm) .and. (w_inverse) & !
           .and. (.not.(w_time))))then
         if((w_all)) distance=dist
         red_inv=  & !
              zofdpm( h_0, omm, omlam, omrad, w_0, distance )
         if(w_verb)then
            write(6,'(a,f9.4,a)')'redshift is', red_inv
         else
            write(6,'(f9.4,$)')red_inv
         endif
      endif


!     /* cosmological time */

      if((w_all) .or. ((.not.(w_inverse)) .and. (w_time)))then
         time_output=  & !
              tofz( h_0, omm, omlam, omrad, w_0, redshift )
         if(w_verb)then
            write(6,'(a,f10.4,a)')'cosmological time',time_output
         else
            write(6,'(f10.4,$)')time_output
         endif
      endif
      
      if((w_all) .or. ((w_inverse) .and. (w_time)))then
         if((w_all)) time=time_output

         red_inv=  & !
              zoft( h_0, omm, omlam, omrad, w_0, time )
         if(w_verb)then
            write(6,'(a,f9.4,a)')'redshift is',red_inv
         else
            write(6,'(f9.4,$)')red_inv
         endif
      endif



!      /* horizon and/or curvature radius */

      if(w_all .or. w_hor)then
         xx= rhoriz( h_0, omm, omlam, omrad, w_0)
         if(w_verb)then
            write(6,'(a,f10.2,a)')'horizon in Mpc = ',xx
         else
            write(6,'(f10.2,$)')xx
         endif
      endif
      if(w_all .or. w_curv)then
         xx= rcurv( h_0, omm, omlam, omrad, w_0)
         if(w_verb)then
            write(6,'(a,f10.2,a)')'curvature radius in Mpc = ',xx
         else
            write(6,'(f10.2,$)')xx
         endif
      endif


!     /*  following all outputs, add an end-of-line */
      if((.not.(w_verb))) print*,' '


      return
      end



      
