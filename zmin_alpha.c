/* 
   zmin_alpha - minimum redshift depending on matched circle size for Poincare model

   Copyright (C) 2011 Boud Roukema

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  

*/

#include <stdio.h>
#include <sys/types.h>

#include <string.h>

#include "system.h"

#ifdef HAVE_ARGP_H
#  include <argp.h>
#endif  /* HAVE_ARGP_H */


#include <stdlib.h>


#define EXIT_FAILURE 1

#if ENABLE_NLS
# include <libintl.h>
# define _(Text) gettext (Text)
#else
# define textdomain(Domain)
# define _(Text) Text
#endif
#define N_(Text) Text

#include <math.h>
#include <gsl/gsl_integration.h>

#include "lib/cosmdist.h" 


char *xmalloc ();
char *xrealloc ();
char *xstrdup ();

#ifdef HAVE_ARGP_H
  static error_t parse_opt (int key, char *arg, struct argp_state *state);
  static void show_version (FILE *stream, struct argp_state *state);
#else
  static void show_version (FILE *stream);
#endif

/* argp option keys */
enum {DUMMY_KEY=129
      ,NOWARN_KEY
};

/* Option flags and variables.  These are initialized in parse_opt.  */

int want_verbose;		/* --verbose */

/* local cosmological parameters */
char interactive_input_string[1024];
double H_0;
double omm;
double omrad;
double omlam;
double w_0;
int    vary_lambda=1; /* vary omlam? otherwise, vary omm */

char ** endptr = NULL;  /*  error handling by   strtod  -  info strtod  */
int  ch;  /* for removing end-of-line after scanf */


#ifdef HAVE_ARGP_H
static struct argp_option options[] =
{
  { "verbose",     'v',           NULL,            0,
    N_("Print more information"), 0 },
  { "H_0",     'h',           N_("HUBBLE_CONSTANT"),            0,
    N_("Hubble constant in km/s/Mpc"), 0 },
  { "omega_matter",     'm',           N_("OMEGA_MATTER"),            0,
    N_("density of non-relativistic Matter"), 0 },
  { "omega_lambda",     'l',           N_("OMEGA_LAMBDA"),            0,
    N_("Lambda - cosmological or quintessence constant"), 0 },
  { "omega_radiation",     'r',           N_("OMEGA_RADIATION"),            0,
    N_("radiation (photons)"), 0 },
  { "w_0",     'q',           N_("QUINTESSENCE_PARAMETER_0"),            0,
    N_("first order Quintessence parameter"), 0 },
  { NULL, 0, NULL, 0, NULL, 0 }
};

/* The argp functions examine these global variables.  */
const char *argp_program_bug_address = "<boud at astro.uni.torun.pl>";
void (*argp_program_version_hook) (FILE *, struct argp_state *) = show_version;

static struct argp argp =
{
  options, parse_opt, N_(" "),
  N_("Poincare dodecahedral space matched disc central redshifts: enter alpha in degrees"),
  NULL, NULL, NULL
};
#endif  /* HAVE_ARGP_H */



int
main (int argc, char **argv)
{

  /*  double H_0, omm, omlam, omrad, w_0;  */
  double input_parameter;
  double alpha=-99.9; /* input value */

  int i_get_zmin; /* return value */

  int want_debug = (0 || COSM_WANT_DEBUG);

  /* useful values to be returned by get_zmin_from_alpha */

  double rSLS;
  double r_curv;

  /* minimum redshift, i.e.  inverse redshift, difference  */
  double z_inverse;  



  textdomain(PACKAGE);

  /* Set up default values.  */
  want_verbose = 0;

  H_0 = 70.0;
  omm = 0.3;
  omlam = 0.7;
  omrad = 1.65e-4 * omm; /* TODO: derive from T_0 etc. */
  w_0 = -1.0;
  
  /*  redshift = 3.0; */

#if HAVE_ARGP_H
  argp_parse(&argp, argc, argv, 0, NULL, NULL);
#else
  printf("Warning: no command line options since a recent glibc is not installed.\n");
#endif  /* HAVE_ARGP_H */


  /* PRELIMINARY work */

  /*  redshift = 3.0;  */

  /* read parameter from standard input */

  if(want_verbose)
    {
      printf("Enter ");
      {
        printf("matched circle radius alpha in deg: ");
      };
    };

  if(want_debug)
    {
      printf("debug1: error reading __%s__ ; errno = %d\n",interactive_input_string,errno);
    };
  
  /* TODO: this bit gives errno as of gfortran/lenny - should be cleaned up */
  scanf("%40s", interactive_input_string);
  while ( (ch=fgetc(stdin)) != '\n' && ch != EOF )
    {
    };

  if(want_debug)
    {
      printf("debug2: string= _%s_  errno = %d\n",interactive_input_string,errno);
    };
  
  errno=0;
  input_parameter= strtod(interactive_input_string,endptr);

  if(errno != 0)
    {
      printf("ERROR: errno= %d  in reading standard input\n",errno);
      printf("interactive_input_string = %s\n",interactive_input_string); /* DEBUG ONLY */
      exit (errno);
    };
  
  if(want_verbose)
    {
    printf("read parameter: %.5f \n",input_parameter);
    };

  
  alpha = input_parameter;


  /*  cosm_dist_init();  This is called by the functions if needed */

  /*
    H_0 = 100.0;
    omm = 1.0;
    omlam = 0.0;
  */

  if(want_verbose)
    {
      printf("H_0= %.2f  Omega_matter= %.5f  Omega_lambda= %.5f  Omega_radiation = %.8f",
	     H_0,omm,omlam,omrad);
      printf(" w_0= %.5f, ",w_0);
      printf(" alpha= %.2f\n",alpha);

    };


  i_get_zmin = cosm_get_zmin_from_alpha
    (  H_0, /* input */
       &omm,  /* input if vary_lambda; otherwise output */
       &omlam, /* output if vary_lambda; otherwise input */
       vary_lambda, /* input: vary omlam? 0/1 = no/yes */
       omrad,  /* input */
       w_0,  /* input */
       alpha, /* input */
       &rSLS, /* output */
       &r_curv, /* output */
       &z_inverse /* output */
       );


  if(vary_lambda){
    if(want_verbose)
      printf("omlam_best is %.5f  rSLS_best is %.5f | omm = %.3f omtot = %.5f\n", 
             omlam,rSLS, omm, omm+omlam+omrad);
  }else{
    if(want_verbose)
      printf("omm_best is %.5f  rSLS_best is %.5f | omlam = %.3f omtot = %.5f\n", 
             omm,rSLS, omlam, omlam+omm+omrad);
  };

  
  if(want_verbose)
    printf("z_min is %.5f\n", z_inverse);
  else
    printf("%.5f\n",z_inverse);

  exit (0);


};

#ifdef HAVE_ARGP_H
/* Parse a single option.  */
static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  switch (key)
    {
    case ARGP_KEY_INIT:
      break;
    case 'v':			/* --verbose */
      want_verbose = 1;
      break;
    case 'h':
      errno=0; H_0= strtod(arg,endptr);
      if(errno != 0)
	{
	  printf("ERROR: errno= %d  in reading H_0 value\n",errno);
	  exit (errno);
	};
      break;
    case 'm':
      errno=0; omm= strtod(arg,endptr);
      if(errno != 0)
	{
	  printf("ERROR: errno= %d  in reading Omega_matter value\n",errno);
	  exit (errno);
	};
      break;
    case 'l':
      errno=0; omlam= strtod(arg,endptr);
      vary_lambda=0; /* omlam becomes fixed if it is given as an input parameter */
      if(errno != 0)
	{
	  printf("ERROR: errno= %d  in reading Omega_Lambda value\n",errno);
	  exit (errno);
	};
      break;
    case 'r':
      errno=0; omrad= strtod(arg,endptr);
      if(errno != 0)
	{
	  printf("ERROR: errno= %d  in reading Omega_radiation value\n",errno);
	  exit (errno);
	};
      break;
    case 'q':
      errno=0; w_0= strtod(arg,endptr);
      if(errno != 0)
	{
	  printf("ERROR: errno= %d  in reading w_0 value\n",errno);
	  exit (errno);
	};
      break;
    case ARGP_KEY_ARG:		/* [FILE]... */
      /* TODO: Do something with ARG, or remove this case and make
         main give argp_parse a non-NULL fifth argument.  */
      break;

    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}
#endif  /* HAVE_ARGP_H */


/* Show the version number and copyright information.  */
static void
#ifdef HAVE_ARGP_H
show_version (FILE *stream, struct argp_state *state)
#else
     show_version (FILE *stream)
#endif  /* HAVE_ARGP_H */
{
#ifdef HAVE_ARGP_H
  (void) state;
#endif  /* HAVE_ARGP_H */
  /* Print in small parts whose localizations can hopefully be copied
     from other programs.  */
  fputs(PACKAGE" "VERSION"\n", stream);
  fprintf(stream, _("Written by %s.\n\n"), "Boud Roukema");
  fprintf(stream, _("Copyright (C) %s %s\n"), "2004", "Boud Roukema");
  fputs(_("\
This program is free software; you may redistribute it under the terms of\n\
the GNU General Public License.  This program has absolutely no warranty.\n"),
	stream);
}
